"""
Joe Kopplin
SL: Natalie Ramirez
11/26/2018
Final Project
This module reads data from csv files that the top 250 players organized by most points and most minutes played
"""

import json
import pandas as pd
import numpy as np, matplotlib.pyplot as plt
from scipy import stats


def read_data(csv):
    """
    Reads the data and returns a dataframe
    """
    df = pd.read_csv(csv, header=0)

    return df

def make_fig(minutes, points):
    """
    Takes two dataframes as arguments and uses them as x and y values to plot a scatter plot with a regression line.
    Each player must have at least 10,000 points scored and 25,000 minutes played in their career to qualify.
    """
    #Generate useable data adn cleaning
    m = pd.merge(minutes, points, on=['Player'], how='outer')
    m.dropna()
    m = m.fillna(method='ffill')
    m = m.iloc[0:262]
    
    x, y = m['MP'], m['PTS']

    #Plot creation
    xi = m['MP'].values
    y1 = m['PTS'].values/m['MP'].values
    
    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    line = (slope*xi) + intercept
    
    plt.plot(x, y, 'o', x, line)
   
    #Axis creation and cleaning
    ax = plt.gca()
    
    ax.set_ylabel('Total Points Scored in Career')

    ax.set_xlabel('Total Minutes Played in Career')

    ax.set_title('Efficency (Player Points/Minutes)')

    return plt.show()

def main():
    points = read_data('career-pts.csv')
    minutes = read_data('career-mp.csv')

    make_fig(minutes, points)

if __name__ == '__main__':
    main()  