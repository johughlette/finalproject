"""
Joe Kopplin
SL: Natalie Ramirez
11/26/2018
Final Project
This module reads data from csv files that containg advanced metrics about career performances from Kareem Abdul-Jabbar, Michael Jordan, and LeBron James. It then plots this calculated data into a bar graph
"""

import json
import pandas as pd
import numpy as np, matplotlib.pyplot as plt
from scipy import stats


def read_data(csv, skiprows):
    """
    Reads the data and creates a dataframe
    """
    
    df = pd.read_csv(csv, header=0, skiprows=skiprows, index_col=0)

    return df

def make_fig(k, mj, l):
    """
    Uses the three dataframes and calculates the win shares per game, win shares per 48 minutes played, and wins above replacement per game.
    It then plots a multibar plot grouped by player.
    """
    
    x = ['Kareem Abdul-Jabbar','Michael Jordan','LeBron James']
    tx = np.arange(len(x))
    
    #Calculating win shares per game played
    k_wpg = (k.loc['Career','WS']/k.loc['Career','G'])
    mj_wpg = (mj.loc['Career','WS']/mj.loc['Career','G'])
    l_wpg = (l.loc['Career','WS']/l.loc['Career','G'])
    
    #Calculating WAR per game
    k_war = (k.loc['Career','VORP']*2.7)/(k.loc['Career','G']-551) #eliminates last few seasons for fairness' sake
    mj_war = (mj.loc['Career','VORP']*2.7)/(mj.loc['Career','G']+177) #accounts for missed seasons 
    l_war = (l.loc['Career','VORP']*2)/(l.loc['Career','G']+99) #eliminates current season
    
    wpg = [k_wpg, mj_wpg, l_wpg]
    wp48 = [k.loc['Career','WS/48'], mj.loc['Career','WS/48'], l.loc['Career','WS/48']-.018]
    war = [k_war, mj_war, l_war]
    
    
    ax = plt.subplot(111)
    #win shares per game
    ax.bar(tx-0.2, wpg, width=0.2, color='b', align='center', label='Win Shares per Game')
    
    #win shares per 48 minutes played
    ax.bar(tx, wp48, width=0.2, color='g', align='center',label='Win Shares per 48 minutes')
    
    #wins above replacement
    ax.bar(tx+0.2, war, width=0.2, color='r', align='center', label='Wins Above Replacement per Game')
    
    
    ax.set_yticks([0.0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3])
    ax.set_xticks(tx, x)
    ax.set_title('Win Data Per Game Played Grouped By Player')
    
    ax.legend()
    
    plt.xticks(tx, x)
    
    return plt.show()

def main():
    kareem_adv = read_data('kareem-adv.csv', [16,17,18,19,20])
    mj_adv = read_data('mj-adv.csv', None)
    lebron_adv = read_data('lebron-adv.csv', [16])
    
    make_fig(kareem_adv, mj_adv, lebron_adv)

if __name__ == '__main__':
    main()