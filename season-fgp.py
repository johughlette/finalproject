"""
Joe Kopplin
SL: Natalie Ramirez
11/26/2018
Final Project
This module reads data from csv files that containg field goal percentage and the number of seasons played in the league. It creates a line plot based on those stats.
"""

import json
import pandas as pd
import numpy as np, matplotlib.pyplot as plt
from scipy import stats

def read_data(csv, num_seasons, skiprows=None):
    """
    Reads the data and makes dataframe
    """
    
    df = pd.read_csv(csv, header=0, skipfooter=1, skiprows=skiprows, index_col=0)
    
    df['Season'] = range(1,num_seasons+1)
    df.index =range(1,num_seasons+1)
    
    return df
    
def make_fig(k, mj, l):
    """
    Makes a line plot of field goal percentages over the number of seasons each player has played in the league
    """

    #kareem
    k['FG%'].plot(color='blue', label='Kareem Abdul-Jabbar')
    
    #mj
    mj['FG%'].plot(color='green', label='Michael Jordan')
    
    #lebron
    l['FG%'].plot(color='red', label='LeBron James')
    
    #Axis creation
    ax = plt.gca()
    ax.set_xticks(range(1, 16))
    ax.set_yticks([0.40, 0.45, 0.50, 0.55, 0.60])
    
    ax.set_xlabel('No. of Seasons in NBA')
    ax.set_ylabel('Field Goal Percentage')
    
    ax.legend()
    
    ax.set_title('Field Goal % Per Season')
    
    return plt.show()
    
def main():
    kareem = read_data('kareem.csv', 15, [16,17,18,19,20])
    mj = read_data('mj.csv', 15, [10,15,16,17])
    lebron = read_data('lebron.csv', 15, [16])

    make_fig(kareem, mj, lebron)
    
    
if __name__ == '__main__':
    main()
